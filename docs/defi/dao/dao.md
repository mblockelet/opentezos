---
id: dao
disable_pagination: true
title: Decentralized Autonomous Organization
author: Daniel Nomadic
---

A DAO is an organization that uses open-source smart contract codes to automate and facilitate decision-making.
Using a DAO may help prevent human error or manipulation since the decision power is no longer in the hands of a few trusted people. Rather, it is automated distributively and written as an immutable code deployed on the blockchain.
Using a DAO is not risk-free: Recall [the DAO project where coding errors led to \$50 million of funds being lost](https://www.coindesk.com/understanding-dao-hack-journalists).

### DAOs on Tezos

- [StakerDao](https://www.stakerdao.com/): a DAO governed by a community of blockchain and ﬁnance enthusiasts who vote to make decisions and govern to build cross-chain ﬁnancial assets. The process of governance is as follows:
New ideas for products and features are proposed on the [StakerDAO forum](https://forum.stakerdao.com/).
StakerDAO’s ecosystem comprises Staker Politicians (individuals involved with governance — a requirement is to hold 1% of \$STKR, StakerDAO’s token—and qualiﬁed to submit proposals), STKR token holders, and the Staker development team.

- [Plenty Governance](https://www.plentydefi.com/vote) to propose, discuss, vote and upgrade.

- [Kolibri DAO](https://governance.kolibri.finance/)
