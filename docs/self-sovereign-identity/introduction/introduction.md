---
id: introduction
title: Introduction
slug: /self-sovereign-identity
authors: Daniel Nomadic
---

Digital and technological tools are ever more embedded in our daily lives. Use of smart objects has soared over recent years, putting a wide range of new and innovative products within easier reach.

In today’s constantly evolving digital ecosystem, we are using more and more apps, services, and digital platforms. When we use these services, we generally have to provide information about our identity. Users often underestimate the importance of controlling their personal data online. This explains why the most commonly used passwords tend to be very simple (for example, “abc123,” birthdays, names of pets and so on).

The more we interact online, the greater our potential exposure to cyber criminals.

Even if we take steps to protect our online data and passwords, we can still be vulnerable to more sophisticated cyber attacks. Many websites and businesses have had thousands or even millions of user passwords and pieces of private information breached by hackers.

History shows that we cannot blindly trust centralized entities to manage our data. This is why it is so important to come up with more resilient, decentralized management solutions, particularly where our identities are concerned.

Self-sovereign identity is a paradigm shift for digital identity, which promises significant advantages but over whose definition consensus has not yet been reached. In this document, we set out the attributes of self-sovereign identity offered by decentralized digital identity, as well as its digital properties.

### Definitions:

**SSI – Self-sovereign Identity**: A cryptographic technology which allows individuals and organizations to confirm their identity. Self-sovereign identity is private and not shared.

**DID – Decentralized Identifier**: Set of identifiers enabling the creation of a verifiable decentralized digital identity. They are based on the SSI paradigm. A DID allows a person, organization, or data model to be identified and verified by a DID controller. These identifiers are designed to allow a DID controller to prove they have control of the DID.

**VCs – Verifiable Credentials**: A verifiable credential consists of information regarding the identification of:
- the subject of said credential (for example, a photo, name, or identification number),
- information relating to the issuing authority (for example, a local council, national agency, or certifying body),
- information regarding the type of document in question (for example, a Dutch passport, a US driving license or a health insurance card),
- information about the specific attributes or properties of the subject requested by the issuing authority (for example, their nationality, the vehicle categories they are licensed to drive, or their date of birth),
- proof of the origins of a legitimation card,
- information about any limits on the card (for example, the expiry date or conditions of use).

A verifiable credential can include the same information as a physical proof of identity (e.g. a driver licence, a fingerprint, or whatever is appropriate). The addition of technologies like digital signatures makes verifiable credentials more secure and reliable than their physical equivalents.

This document aims to give an outline of self-sovereign identities: **decentralized digital identity**, its main characteristics, risks, and the solutions currently available on the Tezos blockchain.